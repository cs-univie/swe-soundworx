# soundworx.net

http://tomcat01lab.cs.univie.ac.at:31999/

## Aufgabenstellung - Soziales Netzwerk

Eine Plattform für ein soziales Netzwerk soll unterschiedliche Dienste für mind. drei verschiedene Benutzergruppen anbieten. Personen soll es z. B. möglich sein, Kontakte zu knüpfen und Informationen zu teilen. Dafür steht z. B. eine Suchfunktion und eine Pinnwand zur Verfügung. Administratoren sollen z. B. unangemessene Beiträge sperren können, bzw. Benutzer (befristet) sperren. ForscherInnen soll es z. B. möglich sein, diverse Statistiken zu Anzahl und Frequenz der Nachrichten, oder dem Vernetzungsgrad von Benutzern, etc., zu erstellen.

## Rahmenbedingungen

Das Softwareprojekt soll mittels Java Servlets/Java Server Pages (JSP) realisiert werden und mittels Apache Tomcat dauerhaft zur Verfügung gestellt werden (auf tomcat01lab.cs.univie.ac.at). Die Datenverwaltung soll mittels eines Data Access Layer umgesetzt und möglichst einfach realisiert werden.
Sämtliche im Rahmen des Projekts erstellten Dateien (Modelle, Beschreibungen, Source Files, Folien, etc.) müssen mittels des Versionskontrollsystems GIT bereitgestellt werden.
Von jedem Teammitglied ist ein persönliches Projekttagebuch zu führen. Jedes Teammitglied muss in allen Phasen der Entwicklung (Use-Case Modellierung, Analyse/Design, Implementierung, Test) eigenständige Beiträge (die auch entsprechend zu dokumentieren sind!) leisten.
Die Projekte müssen auf Basis eines iterativen und inkrementellen Entwicklungsprozesses (siehe Vorlesung) durchgeführt werden. Dabei soll in einer ersten Iteration ein Prototyp realisiert werden, der in einer zweiten Iteration weiterentwickelt wird. Der Entwicklungsprozess muss kontinuierlich während des gesamten Semesters erfolgen.

## GIT

Die Entwicklung hat kontinuierlich und über Git zu erfolgen (Siehe auch Aufgabenstellung)! → Commit/Push aller Teammitglieder in das zentrale Repository (gitta-lab.par.univie.ac.at)
Git ist bestimmungsgemäß zu verwenden! → z.B.: Keine unnötigen "Dateikopien"!
Die Branch "master" hat den für Abgaben relevanten Status zu beinhalten.
Alle Abgabedokumente sind über Git bereitzustellen. Bitte geben Sie auf Ihrer CEWebs-Projektseite deren Pfad an.

Die Ergebnisse jeder Phase sind zu den vorgegebenen Terminen bereitzustellen (GIT, Projekthomepage) und zu präsentieren.

## Projektvorschlag – bis 16.11.2016

 * In Teams zu 4 Personen ist ein Projektvorschlag zu erstellen. Dabei sind die vorgegebenen Richtlinien der Aufgabenstellung zu beachten.
 * Tragen sie sich auf Ihrer Gruppenseite für ein Projekt ein. Siehe: Gruppe1, Gruppe2, Gruppe3, Gruppe4, Gruppe5, Gruppe6, Gruppe7, Gruppe8, Gruppe9, Gruppe10, Gruppe11.
 * Skizzieren sie die grundlegende Funktionalität des Systems aus der externen Sicht seiner BenutzerInnen (mit einem ersten Use-Case Diagramm). Erarbeiten sie mindestens eine zusätzliche Funktionalität, die nicht in der Aufgabenstellung genannt wurde.
 * Finden sie einen Projektnamen.
 * Erstellen sie eine kurze Präsentation über Ihr Projekt. (Dauer max. 10min)
 * Erstellen/Aktualisieren sie die Projekthomepage in CEWebs. Die Homepage soll Informationen über alle Gruppenmitglieder und über das gewählte Projekt enthalten. Weiters muss für alle Dokumente, die im Rahmen des Projekts erstellt werden, aufgeführt sein wo diese abrufbar sind.
 * Erstellen sie in CEWebs ein persönliches Tagebuch in dem alle Projektaktivitäten exakt festgehalten werden können.
 * (zb: Max Mustermann, 10.11.2016, 4h, Spezifizierung Use Case XY). Das Projekttagebuch muss zu jeder Zeit (Abgabegespräche!) in der aktuellen Version verfügbar sein. Beachten sie dabei, dass jedes Teammitglied an Design-Ausarbeitungen und der Implementierung teilnimmt. Dies wird geprüft.


## Anforderungsanalyse I - bis 23.11.2016

 * Erstellen Sie eine Anforderungsanalyse mit der bereitgestellten Vorlage.
 * Erstellen Sie ein Use-Case Modell.
 * Erstellen Sie ein vorläufiges Klassendiagramm.

## Abgabe Design I (Prototyp) - bis 30.11.2016

 * Erstellen Sie ein Designmodell mit der bereitgestellten Vorlage.

## Abgabe Prototyp I und Testfälle (Szenarios) - bis 14.12.2016

 * Implementieren und testen Sie Ihre Anwendung entsprechend dem Design I. 
 * Leiten Sie aus den Use Cases, die Sie umgesetzt haben, Test Cases ab. Dokumentieren Sie jeden Test Case: beschreiben Sie Ziel, Ablauf, Inputdaten, Vorbedingungen und erwartete Outputdaten.
 * Dokumentieren Sie den Java-Code und erstellen Sie mittels javadoc die HTML-Dokumentation. Sie soll Beschreibungen für Packages, Klassen, Methoden und Parameter enthalten. 

## Anforderungsanalyse II, Design II



## Implementierung II / Test II



